import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;

public class SalesAnalytics {

    private List<Sale> saleList = new ArrayList<>();
    private int errorCount = 0;

    public void readFromCSV(String filename) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            br.readLine();
            String line;
            while ((line = br.readLine()) != null) {
                try {
                    Sale sale = Sale.ofCSVString(line);
                    saleList.add(sale);
                } catch (IllegalArgumentException e) {
                    errorCount++;
                }
            }
        }
    }

    public Map<String, List<Sale>> getSalesOfEmployee(){
        Map<String, List<Sale>> salesByEmployee = new HashMap<>();
        for (Sale sale : saleList) {
            String employee = sale.getEmployeeName();
            List<Sale> salesForEmployee = salesByEmployee.get(employee);
            if (salesForEmployee == null) {
                salesForEmployee = new ArrayList<>();
                salesByEmployee.put(employee, salesForEmployee);
            }
            salesForEmployee.add(sale);
        }
        return salesByEmployee;
    }

    public SortedMap<LocalDate, List<Sale>> getSalesPerDay(LocalDate from, LocalDate to){
        SortedMap<LocalDate, List<Sale>> salesByDay = new TreeMap<>();
        for (Sale sale : saleList) {
            LocalDate date = sale.getSaleDate();
            if (date.compareTo(from) >= 0 && date.compareTo(to) < 0) {
                List<Sale> salesForDay = salesByDay.get(date);
                if (salesForDay == null) {
                    salesForDay = new ArrayList<>();
                    salesByDay.put(date, salesForDay);
                }
                salesForDay.add(sale);
            }
        }
        return salesByDay;
    }

    private List<Sale> filter(Predicate<Sale> filter) {
        List<Sale> filteredSales = new ArrayList<>();
        for (Sale sale : saleList) {
            if (filter.test(sale)) {
                filteredSales.add(sale);
            }
        }
        return filteredSales;
    }

    public List<Sale> filterSellerAndBrand(String employee, String brand) {
        Predicate<Sale> filter = sale -> sale.getEmployeeName().equals(employee) && sale.getBrand().equals(brand);
        return filter(filter);
    }

    public static <T> void printSales(Map<T, List<Sale>> sales) {
        for (Map.Entry<T, List<Sale>> entry : sales.entrySet()) {
            System.out.println(entry.getKey());
            for (Sale sale : entry.getValue()) {
                System.out.println("    " + sale);
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        // TODO you can try your methods here.
    }














    // Can't touch this
    /////////////////////////////////////////////////////////////////////////////
    public List<Sale> getSaleList() {
        return saleList;
    }

    public void setSaleList(List<Sale> sales){
        this.saleList = new ArrayList<>(sales);
    }

}
