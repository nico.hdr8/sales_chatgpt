import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class Sale {

    private LocalDate saleDate;
    private double price;
    private String employeeName;
    private String brand;
    private String category;


    public static Sale ofCSVString(String csv) throws IllegalArgumentException {
        String[] values = csv.split(",");
        if (values.length != 5) {
            throw new IllegalArgumentException("Invalid CSV string: " + csv);
        }
        String employeeName = values[0];
        LocalDate saleDate;
        try {
            saleDate = LocalDate.parse(values[1].trim());
        } catch (DateTimeParseException e) {
            throw new IllegalArgumentException("Invalid sale date in CSV string: " + csv);
        }
        double price = Double.parseDouble(values[2].trim());
        String category = values[3].trim();
        String brand = values[4].trim();
        return new Sale(saleDate, price, employeeName, category, brand);
    }


    public Sale(LocalDate saleDate, double price, String employeeName,String category, String brand) {
        if (price < 0) {
            throw new IllegalArgumentException("Price cannot be negative.");
        }

        this.employeeName = employeeName;
        this.saleDate = saleDate;
        this.price = price;
        this.category = category;
        this.brand = brand;
    }

    public LocalDate getSaleDate() {
        return saleDate;
    }

    public double getPrice() {
        return price;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public String getBrand() {
        return brand;
    }

    public String getCategory() {
        return category;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "saleDate=" + saleDate +
                ", price=" + price +
                ", employeeName='" + employeeName + '\'' +
                ", brand='" + brand + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}
