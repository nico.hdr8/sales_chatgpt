import java.time.LocalDate;
import java.util.List;

public class SalesData {
    public static List<Sale> sales = List.of(new Sale(LocalDate.of(2022,11,30),40004,"Werner", "Traktor", "John Deer"),
            new Sale(LocalDate.of(2022,11,29),40404,"Werner", "Traktor", "John Deer"),
            new Sale(LocalDate.of(2022,11,28),50304,"Werner", "Traktor", "Valtra"),
            new Sale(LocalDate.of(2022,11,27),5004,"Werner", "Anhaenger", "Steyr"),
            new Sale(LocalDate.of(2022,11,28),5404,"Christoph", "Anhaenger", "Steyr"),
            new Sale(LocalDate.of(2022,11,27),123442,"Christoph", "Maehdrescher", "Steyr"));
}
